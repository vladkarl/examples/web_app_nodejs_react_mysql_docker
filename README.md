# Web App example based on NodeJS, React, MySQL and Docker

This is a simple web app (backend and frontend),
which allows the user to register and login 
using API calls on the backend.


## Setup summary

### Prerequisites

1. Internet
1. Software:
   * docker version 20.10+;
   * docker-compose version 1.27+;

### Setup

1. Create the database with the help of db.sql in the container db
1. Set a strong password for the user "app" and put it into app-api/config/db.js


## Run

1. docker-compose up -d
1. http://localhost:3000/


## Logs

1. docker-compose logs -f
