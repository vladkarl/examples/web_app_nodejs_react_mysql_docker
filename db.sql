CREATE DATABASE app;
 
CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  firstname varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  lastname varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  email varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  password varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY email (email)
 ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE USER 'app'@'%' IDENTIFIED WITH mysql_native_password AS 'w2L9hYc3B';
GRANT USAGE ON *.* TO 'app'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;GRANT ALL PRIVILEGES ON `app`.* TO 'app'@'%';
