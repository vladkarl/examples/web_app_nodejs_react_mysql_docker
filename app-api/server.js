const createError = require('http-errors');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const usrRouter = require('./routes/user.js');
 
const app = express();
const port = 3001;
 
app.use(express.json());
 
app.use(bodyParser.json());
 
app.use(bodyParser.urlencoded({
    extended: true
}));
 
app.use(cors());
 
app.use('/api/user', usrRouter);
 
// Handling Errors
app.use((err, req, res, next) => {
    console.log(err);
    console.log("msg1"); 
    err.statusCode = err.statusCode || 500;
    err.message = err.message || "Internal Server Error";
    res.status(err.statusCode).json({
      message: err.message,
    });
});
 
app.listen(port,() => console.log(`Server is running on port ${port}`));
