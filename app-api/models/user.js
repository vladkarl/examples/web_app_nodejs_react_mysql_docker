const db  = require('./index');


const userModel = {
    isUserExist: function (email, cbRtn) {
        db.query(
            `SELECT id FROM users WHERE LOWER(email) = LOWER(${db.escape(email)});`,
            (err, result) => {
                if (err) return cbRtn(err, false);
                if (result[0] && 'id' in result[0]) return cbRtn(null, true);
                return cbRtn(null, false);
            }
        );
    },

    insertUser: function (user, cbRtn) {
        db.query(
            `INSERT INTO users (firstname, lastname, email, password) VALUES (
                  ${db.escape(user.firstname)}, ${db.escape(user.lastname)}, 
                  ${db.escape(user.email)}, ${db.escape(user.password)})`,
            (err, result) => {
                if (err) return cbRtn(err, false);
                return cbRtn(null, true);
            }
        );
    },

    getUser: function (email, cbRtn) {
        db.query(
            `SELECT * FROM users WHERE email = ${db.escape(email)};`,
            (err, result, fields) => {
                if (err) return cbRtn(err, false, null);
                if (result[0] && 'id' in result[0] && result[0].id > 0) return cbRtn(null, true, result[0]);
                return cbRtn(null, false, null);
            }
        );
    }

}


module.exports = userModel;
