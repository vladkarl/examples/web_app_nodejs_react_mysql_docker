var mysql = require('mysql');
const dbConfig = require('../config/db');


var db = mysql.createConnection(dbConfig);
 
db.connect(function(err) {
    if (err) console.log(err);
    else console.log('Connection to DB was done successfully!');
});


module.exports = db;
