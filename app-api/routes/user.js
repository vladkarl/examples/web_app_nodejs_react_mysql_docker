const express = require('express');
const router = express.Router();
const { regValidation, loginValidation } = require('./validations/user');
const usrService = require('../services/user');


router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

router.post('/register', regValidation, (req, res, next) => {
    usrService.register(req.body.user, res, next);
});

router.post('/login', loginValidation, (req, res, next) => {
    usrService.login(req.body.user, res, next);
});
 
 
module.exports = router;
