const { check, validationResult } = require('express-validator');
 
exports.regValidation = [
    check('user.firstname', 'First name is requied & should be at least 2 characters long.').trim().isLength({ min: 2 }),
    check('user.lastname', 'Last name is requied & should be at least 2 characters long.').trim().isLength({ min: 2 }),
    check('user.email', 'Please use a valid email.').trim().toLowerCase().normalizeEmail({ gmail_remove_dots: true }).isEmail(),
    check('user.password', 'Password must be 8+ characters with at least one number and one char.').trim().isLength({ min: 8 }).matches(/(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z]+/, "i"),
    check('user.password2', 'Repeat password is not the same as Password.').trim().custom((value, { req }) => value === req.body.user.password),
  (req, res, next) => {
    const resValid = validationResult(req);
    if (!resValid.isEmpty()) {
        let aErrs = resValid.mapped();
        if (aErrs['user.password']) aErrs['user.password'].value = '';
        if (aErrs['user.password2']) aErrs['user.password2'].value = '';
        return res.status(422).json({vldErrors: aErrs});
    }
    next();
  }
]

exports.loginValidation = [
    check('user.email', 'Please use a valid email.').trim().toLowerCase().normalizeEmail({ gmail_remove_dots: true }).isEmail(),
    check('user.password', 'Password must be 8+ characters.').trim().isLength({ min: 8 }),
  (req, res, next) => {
    const resValid = validationResult(req);
    if (!resValid.isEmpty()) {
        let aErrs = resValid.mapped();
        if (aErrs['user.password']) aErrs['user.password'].value = '';
        return res.status(422).json({vldErrors: aErrs});
    }
    next();
  }
]
