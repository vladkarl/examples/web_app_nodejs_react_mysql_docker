const db  = require('../models/index');
const usrModel  = require('../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const userService = {
    register: function (user, res, next) {
        console.log('/api/user/register was called for ', user.email);

        usrModel.isUserExist(user.email, (err, b) => {
            if (err) return res.status(500).json({sysError: err.message});
            if (b) return res.status(409).json({appError: 'This user already exist!'});

            bcrypt.hash(user.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({sysError: err.message});
                } else {
                    user.password = hash;
                    usrModel.insertUser(user, (err, b) => {
                        if (err) {
                            // throw err;
                            return res.status(500).json({sysError: err.message});
                        }
                        return res.status(200).json({appMsg: 'The user has been registered!'});
                    });
                }
            });
        });
    },

    login: function (user, res, next) {
        console.log('/api/user/login was called for ', user.email);

        usrModel.getUser(user.email, (err, b, mUser) => {
            if (err) return res.status(500).json({sysError: err.message});
            if (!b) return res.status(409).json({appError: 'The email/password combination is wrong'});

            bcrypt.compare(user.password, mUser.password, (err, b) => {
                if (err) {
                    return res.status(500).json({sysError: err.message});
                } else {
                    if (b) {
                        delete mUser.password;
                        return res.status(200).json({appMsg: 'Logined!', user: mUser});
                    } else {
                        return res.status(409).json({appError: 'The email/password combination is wrong '});
                    }
                }
            });
        });
    }
}


module.exports = userService;
