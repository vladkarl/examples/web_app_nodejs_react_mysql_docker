
export async function srvUser(action, data) {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/user/${action}`, {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({user: data})
    })

    let jResponse = await response.json();
    jResponse['status'] = response.status;

    return jResponse;
}
