import React from 'react';
import { Link } from 'react-router-dom'
import '../../App.css'


export default function LogoutPage({user}) {
    return (
        <div>
            <h1>Data Platform</h1>
            <h3>Goodbye {user.firstname} {user.lastname}!</h3>
            <div><Link to="/">Home</Link></div>
        </div>
    )
}
