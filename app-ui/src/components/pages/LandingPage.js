import React from 'react';
import { Link } from 'react-router-dom'
import '../../App.css'


export default function LandingPage({user, logoutUser}) {
    if (user.isValid) {
        return (
            <div>
                <h1>Data Platform</h1>
                <h3>Welcome {user.firstname} {user.lastname}!</h3>
                <div>To logout click <Link to="/logout" onClick={() => logoutUser()}>here.</Link></div>
            </div>
        )
    } else {
        return (
            <div>
                <h1>Data Platform</h1>
                <p>Join us now to get access to all resources... )</p>
                <div>
                    <Link to="/login">Login</Link>
                    <span> &bull; </span>
                    <Link to="/register">Register</Link>
                </div>
            </div>
        )
    }
}
