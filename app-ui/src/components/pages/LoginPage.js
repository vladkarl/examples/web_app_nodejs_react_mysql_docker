import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Navigate } from 'react-router-dom'
import { srvUser } from '../../services/User'
import { getErrors } from '../../helpers/response'


class LoginPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            arErr: [],
            bRedirect: false,
            isLogin: false
        };
    }
  
    loginUser = (e) => {
        e.preventDefault();
        
        srvUser('login', this.state.user)
            .then(response => {
                let arErr = getErrors(response);

                if (arErr.length > 0) {
                    this.setState({arErr: arErr});
                } else {
                    response['user']['isValid'] = true;
                    this.props.setUserData(response['user']);
                    this.setState({bRedirect: true});
                }
            });

        return false;
    }
  
    onChangeForm = (e) => {
        let user = this.state.user

        if (e.target.name === 'email') {
            user.email = e.target.value;
        } else if (e.target.name === 'password1') {
            user.password = e.target.value;
        }
        
        this.setState({user})
    }
  
    render() {
        if (this.state.bRedirect) {
            return <Navigate to='/'/>;
        }

        return (
            <div className="">
                <div className="">
                    <h2>Login</h2>
                    <div id="divErr">
                        <ul className="listErr">
                            {this.state.arErr.map((item, idx) => (
                                <li key={idx}>{item}</li>
                            ))}
                        </ul>
                    </div>
                    <form onSubmit = {(e) => this.loginUser(e)} method="post">
                        <div className="grid-container">
                            <div className="">
                                <label className="Form-label" htmlFor="email">Email: </label>
                            </div>
                            <div className="">
                                <input type="text" onChange={(e) => this.onChangeForm(e)} name="email" id="email" placeholder="Email" required />
                            </div>
                            <div className="">
                                <label htmlFor="password1">Password: </label>
                            </div>
                            <div className="">
                                <input type="password" onChange={(e) => this.onChangeForm(e)} name="password1" id="password1" required />
                            </div>
                        </div>
                        <div>
                            <button type="submit">Login</button>
                        </div>
                    </form>
                </div>
                <div>&nbsp;</div>
                <Link to="/">Home</Link>
            </div>
        );
    }
  }
  
  export default LoginPage;
