import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { Navigate } from 'react-router-dom'
import { srvUser } from '../../services/User'
import { getErrors } from '../../helpers/response'


class RegisterUserPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            arErr: [],
            bRedirect: false,
            isLogin: false
        };
    }
  
    registerUser = (e) => {
        e.preventDefault();
        
        srvUser('register', this.state.user)
            .then(response => {
                let arErr = getErrors(response);

                if (arErr.length > 0) {
                    this.setState({arErr: arErr});
                } else {
                    this.props.setUserData({firstname: this.state.user.firstname, lastname: this.state.user.lastname, isValid: true});
                    this.setState({bRedirect: true});
                }
            });

        return false;
    }
  
    onChangeForm = (e) => {
        let user = this.state.user

        if (e.target.name === 'firstname') {
            user.firstname = e.target.value;
        } else if (e.target.name === 'lastname') {
            user.lastname = e.target.value;
        } else if (e.target.name === 'email') {
            user.email = e.target.value;
        } else if (e.target.name === 'password1') {
            user.password = e.target.value;
        } else if (e.target.name === 'password2') {
            user.password2 = e.target.value;
        }
        
        this.setState({user})
    }
  
    render() {
        if (this.state.bRedirect) {
            return <Navigate to='/'/>;
        }

        return (
            <div className="">
                <div className="">
                    <h2>Register User</h2>
                    <div id="divErr">
                        <ul className="listErr">
                            {this.state.arErr.map((item, idx) => (
                                <li key={idx}>{item}</li>
                            ))}
                        </ul>
                    </div>
                    <form onSubmit = {(e) => this.registerUser(e)} method="post">
                        <div className="grid-container">
                            <div className="">
                                <label className="Form-label" htmlFor="firstname">First Name: </label>
                            </div>
                            <div className="">
                                <input type="text" onChange={(e) => this.onChangeForm(e)} name="firstname" id="firstname" placeholder="First Name" required />
                            </div>
                            <div className="">
                                <label htmlFor="lastname">Last Name: </label>
                            </div>
                            <div className="">
                                <input type="text" onChange={(e) => this.onChangeForm(e)} name="lastname" id="lastname" placeholder="Last Name" required />
                            </div>
                            <div className="">
                                <label className="Form-label" htmlFor="email">Email: </label>
                            </div>
                            <div className="">
                                <input type="text" onChange={(e) => this.onChangeForm(e)} name="email" id="email" placeholder="Email" required />
                            </div>
                            <div className="">
                                <label htmlFor="password1">Password: </label>
                            </div>
                            <div className="">
                                <input type="password" onChange={(e) => this.onChangeForm(e)} name="password1" id="password1" required />
                            </div>
                            <div className="">
                                <label htmlFor="password2">Repeat password: </label>
                            </div>
                            <div className="">
                                <input type="password" onChange={(e) => this.onChangeForm(e)} name="password2" id="password2" required />
                            </div>
                        </div>
                        <div>
                            <button type="submit">Register</button>
                        </div>
                    </form>
                </div>
                <div>&nbsp;</div>
                <Link to="/">Home</Link>
            </div>
        );
    }
  }
  
  export default RegisterUserPage;
