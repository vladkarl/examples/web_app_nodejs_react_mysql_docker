import logo from './logo.svg';
import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import LandingPage from './components/pages/LandingPage'
import LoginPage from './components/pages/LoginPage'
import RegisterUserPage from './components/pages/RegisterUserPage'
import LogoutPage from './components/pages/LogoutPage'


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {}
        };
    }

    setUserData = (user) => {
        this.setState({
            user: user
        });
    }

    logoutUser = () => {
        this.setState(prevState => ({
            user: {
                ...prevState.user,
                isValid: false
            }
        }));
    }

    render() {
        return (
            <Router>
                <div className="App">
                    <header className="App-header">
                        <img src={logo} className="App-logo" alt="logo" />
                    </header>
                    <Routes>
                        <Route exact path="/" element={<LandingPage user={this.state.user} logoutUser={this.logoutUser} />} />
                        <Route path="/login" element={<LoginPage setUserData={this.setUserData} />} />
                        <Route path="/register" element={<RegisterUserPage setUserData={this.setUserData} />} />
                        <Route path="/logout" element={<LogoutPage user={this.state.user} />} />
                    </Routes>
                </div>
            </Router>
        );
    }
}

export default App;
