
export function getErrors(response) {
    let errs = response['vldErrors'], arErr = [];

    for (let key in errs) {
        if (errs.hasOwnProperty(key)) {
            if (errs[key]['msg'].length > 0) {
                arErr.push(errs[key]['msg']);
            }
        }
    }

    if (response['appError']) arErr.push(response['appError']);
    if (response['status'] !== 200 && arErr.length === 0) arErr.push('An error!');

    return arErr;
}
